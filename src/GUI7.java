import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.border.EtchedBorder;
import javax.swing.border.TitledBorder;

public class GUI7 {
	BankAccount obj = new BankAccount(5000);
	String show = "5000.0\n";
	public GUI7(){
		JFrame f = new JFrame();
		
		JPanel p1 = new JPanel();
		JPanel p2 = new JPanel();
		JPanel p3 = new JPanel();
		
		f.setSize(515, 350);
		p1.setPreferredSize(new Dimension(250,300));
		p2.setPreferredSize(new Dimension(250,300));
		p3.setPreferredSize(new Dimension(250,150));
		p1.setBorder(new TitledBorder(new EtchedBorder(), "Deposit"));
		p2.setBorder(new TitledBorder(new EtchedBorder(), "Withdraw"));
		p3.setBorder(new TitledBorder(new EtchedBorder(), "You're money"));
		
		p1.setBackground((new Color(190,225,100)));
		p2.setBackground(Color.pink);
		p3.setBackground((new Color(150,200,210)));
		
		JButton b1 = new JButton("Submit");
		JButton b2 = new JButton("Submit");
		
		JTextField t1 = new JTextField();
		JTextField t2 = new JTextField();

		t1.setPreferredSize(new Dimension(150,80));
		t2.setPreferredSize(new Dimension(150,80));
		
		JLabel label1 = new JLabel("Enter money", SwingConstants.CENTER);
		JLabel label2 = new JLabel("Enter money", SwingConstants.CENTER);
		
		JTextArea area3 = new JTextArea();
		area3.setPreferredSize(new Dimension(150,80));
		
		b1.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e){
				if (e.getSource() == b1) {
					String dep = t1.getText();
					int getDep = Integer.parseInt(dep);
					obj.deposit(getDep);
					show += obj.getBalance()+"\n";
					area3.setText(show);
				}
			}
	});
		b2.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e){
				if (e.getSource() == b2) {
					String wit = t2.getText();
					int getWit = Integer.parseInt(wit);
					obj.withdraw(getWit);
					show += obj.getBalance()+"\n";
					area3.setText(show);
				}
			}
	});
		area3.setText("5000.0");
		p3.add(area3, BorderLayout.CENTER);
		p1.add(label1,BorderLayout.NORTH);
		p1.add(t1,BorderLayout.SOUTH);
		
		p2.add(label2,BorderLayout.NORTH);
		p2.add(t2,BorderLayout.SOUTH);
		
		p1.add(b1, BorderLayout.SOUTH);
		p2.add(b2, BorderLayout.SOUTH);
		f.add(p1, BorderLayout.WEST);
		f.add(p2, BorderLayout.EAST);
		f.add(p3, BorderLayout.SOUTH);
	
		f.setVisible(true);
		f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	
	}

	

}