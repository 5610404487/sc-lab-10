import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.border.EtchedBorder;
import javax.swing.border.TitledBorder;

public class GUI5 {
	public GUI5(){
		JFrame f = new JFrame();

		JPanel p1 = new JPanel();
		JPanel bg = new JPanel();
		f.setSize(400, 400);
		p1.setPreferredSize(new Dimension(400,400));
		bg.setPreferredSize(new Dimension(200,300));
		p1.setBorder(new TitledBorder(new EtchedBorder(), "Screen"));
		p1.setBackground(Color.pink);
		bg.setBackground(Color.gray);
		
		JMenuBar menuBar = new JMenuBar();
		JMenu color = new JMenu("Edit Background");
		JMenuItem r = new JMenuItem("Red");
		JMenuItem g = new JMenuItem("Green");
		JMenuItem b = new JMenuItem("Blue");		
		
		r.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e){
				if (e.getSource() == r) {
					bg.setBackground(Color.red);
				}
			}
	});
		g.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e){
				if (e.getSource() == g) {
					bg.setBackground(Color.green);
				}
			}
	});
		b.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e){
				if (e.getSource() == b) {
					bg.setBackground(Color.blue);
				}
			}
	});
		
		color.add(r);
		color.add(g);
		color.add(b);
		menuBar.add(color);
		p1.add(bg, BorderLayout.SOUTH);
		f.setJMenuBar(menuBar);
		f.add(p1, BorderLayout.CENTER);
	
		f.setVisible(true);
		f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
	}
}
