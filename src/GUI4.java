import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EtchedBorder;
import javax.swing.border.TitledBorder;

public class GUI4 {
	public GUI4(){
		JFrame f = new JFrame();
		
		JPanel p1 = new JPanel();
		JPanel p2 = new JPanel();
		JPanel bg = new JPanel();
		f.setSize(400, 400);
		p1.setPreferredSize(new Dimension(400,300));
		p2.setPreferredSize(new Dimension(400,100));
		bg.setPreferredSize(new Dimension(200,200));
		p1.setBorder(new TitledBorder(new EtchedBorder(), "Screen"));
		p2.setBorder(new TitledBorder(new EtchedBorder(), "Button"));
		p1.setBackground(Color.lightGray);
		p2.setBackground(Color.pink);
		bg.setBackground(Color.gray);
		JComboBox rgb = new JComboBox();
		rgb.addItem("Red");
		rgb.addItem("Green");
		rgb.addItem("Blue");
		
		rgb.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent arg0){
				int index = rgb.getSelectedIndex();
				index = index+1;
				if(index==1){
					bg.setBackground(Color.red);
				}
				if(index==2){
					bg.setBackground(Color.green);
				}
				if(index==3){
					bg.setBackground(Color.blue);
				}
			}
		});
		
		p1.add(bg, BorderLayout.SOUTH);
		p2.add(rgb, BorderLayout.SOUTH);
		f.add(p1, BorderLayout.CENTER);
		f.add(p2, BorderLayout.SOUTH);
	
		f.setVisible(true);
		f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
	}
}
