import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.ButtonGroup;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.border.EtchedBorder;
import javax.swing.border.TitledBorder;


public class GUI2 {
	public GUI2(){
		JFrame f = new JFrame();
		
		JPanel p1 = new JPanel();
		JPanel p2 = new JPanel();
		JPanel bg = new JPanel();
		f.setSize(400, 400);
		p1.setPreferredSize(new Dimension(400,300));
		p2.setPreferredSize(new Dimension(400,100));
		bg.setPreferredSize(new Dimension(200,200));
		p1.setBorder(new TitledBorder(new EtchedBorder(), "Screen"));
		p2.setBorder(new TitledBorder(new EtchedBorder(), "Button"));
		p1.setBackground(Color.lightGray);
		p2.setBackground(Color.pink);
		bg.setBackground(Color.gray);
		
		JRadioButton b1 = new JRadioButton("Red");
		JRadioButton b2 = new JRadioButton("Green");
		JRadioButton b3 = new JRadioButton("Blue");
		ButtonGroup group = new ButtonGroup();
		group.add(b1);
		group.add(b2);
		group.add(b3);
		
		b1.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e){
				if (b1.isSelected()) {
					bg.setBackground(Color.red);
				}
			}
	});
		b2.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e){
				if (b2.isSelected()) {
					bg.setBackground(Color.green);
				}
			}
	});
		b3.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e){
				if (b3.isSelected()) {
					bg.setBackground(Color.blue);
				}
			}
	});

		p1.add(bg, BorderLayout.SOUTH);
		p2.add(b1, BorderLayout.SOUTH);
		p2.add(b2, BorderLayout.SOUTH);
		p2.add(b3, BorderLayout.SOUTH);
		f.add(p1, BorderLayout.CENTER);
		f.add(p2, BorderLayout.SOUTH);
	
		f.setVisible(true);
		f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
	}
}
