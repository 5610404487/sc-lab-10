import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EtchedBorder;
import javax.swing.border.TitledBorder;


public class GUI1 {
	public GUI1(){
		JFrame f = new JFrame();
		
		JPanel p1 = new JPanel();
		JPanel p2 = new JPanel();
		JPanel bg = new JPanel();
		f.setSize(400, 400);
		p1.setPreferredSize(new Dimension(400,300));
		p2.setPreferredSize(new Dimension(400,100));
		bg.setPreferredSize(new Dimension(200,200));
		p1.setBorder(new TitledBorder(new EtchedBorder(), "Screen"));
		p2.setBorder(new TitledBorder(new EtchedBorder(), "Button"));
		p1.setBackground(Color.lightGray);
		p2.setBackground(Color.pink);
		bg.setBackground(Color.gray);
		
		JButton b1 = new JButton("Red");
		JButton b2 = new JButton("Green");
		JButton b3 = new JButton("Blue");
		
		b1.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e){
				if (e.getSource() == b1) {
					bg.setBackground(Color.red);
				}
			}
	});
		b2.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e){
				if (e.getSource() == b2) {
					bg.setBackground(Color.green);
				}
			}
	});
		b3.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e){
				if (e.getSource() == b3) {
					bg.setBackground(Color.blue);
				}
			}
	});

		p1.add(bg, BorderLayout.SOUTH);
		p2.add(b1, BorderLayout.SOUTH);
		p2.add(b2, BorderLayout.SOUTH);
		p2.add(b3, BorderLayout.SOUTH);
		f.add(p1, BorderLayout.CENTER);
		f.add(p2, BorderLayout.SOUTH);
	
		f.setVisible(true);
		f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
	}
}
