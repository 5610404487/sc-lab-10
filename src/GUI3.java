import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JCheckBox;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EtchedBorder;
import javax.swing.border.TitledBorder;

public class GUI3 {
	int r = 0;
	int g = 0;
	int b = 0;
	public GUI3(){
		JFrame f = new JFrame();
		
		JPanel p1 = new JPanel();
		JPanel p2 = new JPanel();
		JPanel bg = new JPanel();
		f.setSize(400, 400);
		p1.setPreferredSize(new Dimension(400,300));
		p2.setPreferredSize(new Dimension(400,100));
		bg.setPreferredSize(new Dimension(200,200));
		p1.setBorder(new TitledBorder(new EtchedBorder(), "Screen"));
		p2.setBorder(new TitledBorder(new EtchedBorder(), "Button"));
		p1.setBackground(Color.lightGray);
		p2.setBackground(Color.pink);
		bg.setBackground(Color.gray);
		
		JCheckBox b1 = new JCheckBox("Red");
		JCheckBox b2 = new JCheckBox("Green");
		JCheckBox b3 = new JCheckBox("Blue");
		bg.setBackground(new Color(r,g,b));
		
		b1.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e){
				if (b1.isSelected()) {
					r=225;
					bg.setBackground(new Color(r,g,b));
					} 
				else {
					r=0;
					bg.setBackground(new Color(r,g,b));
				}
			}
	});
		b2.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e){
				if (b2.isSelected()) {
					g=225;
					bg.setBackground(new Color(r,g,b));
					} 
				else {
					g=0;
					bg.setBackground(new Color(r,g,b));
				}
			}
	});
		b3.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e){
				if (b3.isSelected()) {
					b=225;
					bg.setBackground(new Color(r,g,b));
					} 
				else {
					b=0;
					bg.setBackground(new Color(r,g,b));
				}
				
			}
	});
		p1.add(bg, BorderLayout.SOUTH);
		p2.add(b1, BorderLayout.SOUTH);
		p2.add(b2, BorderLayout.SOUTH);
		p2.add(b3, BorderLayout.SOUTH);
		f.add(p1, BorderLayout.CENTER);
		f.add(p2, BorderLayout.SOUTH);
	
		f.setVisible(true);
		f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
	}
}
